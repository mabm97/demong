/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import mx.chineloio.conexion.ConexionBD;
import mx.chineloio.modelo.Usuario;

/**
 *
 * @author k-m-f
 */
public class UsuarioDao {

    public Usuario iniciarSesion(String email, String contra) throws SQLException, Exception {
        String consulta = "Select u.*,r.* FROM usuario AS u LEFT JOIN rol AS r ON u.rol_id = \n "
                + " r.id WHERE email='" + email + "' and contrasena='" + contra + "'  ";
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        Usuario us = null;
        try {
            con = new ConexionBD().getConexion();
            st = con.createStatement();
            rs = st.executeQuery(consulta);
            us = new Usuario();
            while (rs.next()) {
                us.setId(rs.getString("id"));
                us.setNombre(rs.getString("nombre"));
                us.setEmail(rs.getString("email"));
                us.setContrasena(rs.getString("telefono"));
                us.setIdRole(rs.getString("rol_id"));
                us.setIdRole(rs.getString("descripcion"));
            }
        } catch (Exception e) {
            throw new Exception("Ocurrió un error en la Base de Datos al realizar la sesión.", e);
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                rs.close();
            }
            if (con != null) {
                con.close();
            }
            return us;
        }

    }

    public Boolean saveUsuario(Usuario us) throws SQLException {
        Connection con = null;
        Statement st = null;
        Boolean resultado = false;
        String consulta = "INSERT INTO usuario (nombre,email,contrasena,telefono,rol_id,direccion) VALUES \n"
                + " ('" + us.getNombre() + "','" + us.getEmail() + "','" + us.getContrasena() + "','" + us.getTelefono() + "','"+us.getIdRole()+"','"+us.getDireccion()+"' )";
        try {
            con = new ConexionBD().getConexion();
            con.setAutoCommit(false);
            st = con.createStatement();
            st.executeUpdate(consulta);
            resultado = true;
            con.commit();
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
        } finally {
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }
            return resultado;
        }
    }

   
}
