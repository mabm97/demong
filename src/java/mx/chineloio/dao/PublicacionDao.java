/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import mx.chineloio.conexion.ConexionBD;
import mx.chineloio.modelo.Publicacion;

/**
 *
 * @author k-m-f
 */
public class PublicacionDao {

    public Boolean crearPublicacion(Publicacion pub) throws SQLException {
        Boolean resultado = false;
        Connection con = null;
        Statement st = null;
        String consulta = "INSERT INTO publicacion (descripcion,cantidad,titulo,usuario_id) VALUES \n"
                + " ('" + pub.getDescripcion() + "','" + pub.getCantidad() + "','" + pub.getTitulo() + "', '" + pub.getUsuario_id() + "' )";
        try {
            con = new ConexionBD().getConexion();
            con.setAutoCommit(false);
            st = con.createStatement();
            st.executeUpdate(consulta);
            resultado = true;
            con.commit();
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
        } finally {
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }
            return resultado;
        }
    }

    public Boolean quitarPublicacion(String id) throws SQLException {
        Boolean resultado = false;
        Connection con = null;
        Statement st = null;
        String consulta = "DELETE publicacion FROM publicacion WHERE id = "+id;
        try {
            con = new ConexionBD().getConexion();
            con.setAutoCommit(false);
            st = con.createStatement();
            st.executeUpdate(consulta);
            resultado = true;
            con.commit();
        } catch (Exception e) {
            if (con != null) {
                con.rollback();
            }
        } finally {
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }
            return resultado;
        }
    }

    public ArrayList<Publicacion> obtenerPublicaciones(String idUsuario) throws SQLException {
        ArrayList<Publicacion> resultado = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String consulta = "SELECT * FROM `publicacion` WHERE usuario_id = " + idUsuario;
        try {
            con = new ConexionBD().getConexion();
            st = con.createStatement();
            rs = st.executeQuery(consulta);
            resultado = new ArrayList<>();
            while (rs.next()) {
                Publicacion pub = new Publicacion();
                pub.setId(rs.getString("id"));
                pub.setDescripcion(rs.getString("descripcion"));
                pub.setCantidad(rs.getString("cantidad"));
                pub.setTitulo(rs.getString("titulo"));
                resultado.add(pub);
            }
        } catch (Exception e) {
            throw new Exception("Error al obtener las publicaciones del usuario", e);
        } finally {
            if (st != null) {
                st.close();
            }
            if (con != null) {
                con.close();
            }
            return resultado;
        }
    }

}
