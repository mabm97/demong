/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.modelo;

/**
 *
 * @author k-m-f
 */
public class Cita {

    private String id;
    private String fechaCita;
    private String estado;
    private String publicacionId;
    private String usuarioId;

    public Cita(String id, String fechaCita, String estado, String publicacionId, String usuarioId) {
        this.id = id;
        this.fechaCita = fechaCita;
        this.estado = estado;
        this.publicacionId = publicacionId;
        this.usuarioId = usuarioId;
    }

    public Cita() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPublicacionId() {
        return publicacionId;
    }

    public void setPublicacionId(String publicacionId) {
        this.publicacionId = publicacionId;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

}
