/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.modelo;

/**
 *
 * @author k-m-f
 */
public class Usuario {

    private String id;
    private String email;
    private String contrasena;
    private String telefono;
    private String nombre;
    private String idRole;
    private String descripcionRole;
    private String direccion;

    public Usuario() {
    }

    public Usuario(String id, String email, String contrasena, String telefono, String nombre, String idRole, String descripcionRole) {
        this.id = id;
        this.email = email;
        this.contrasena = contrasena;
        this.telefono = telefono;
        this.nombre = nombre;
        this.idRole = idRole;
        this.descripcionRole = descripcionRole;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    public String getDescripcionRole() {
        return descripcionRole;
    }

    public void setDescripcionRole(String descripcionRole) {
        this.descripcionRole = descripcionRole;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
