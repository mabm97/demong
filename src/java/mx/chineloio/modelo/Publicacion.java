/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.modelo;

/**
 *
 * @author k-m-f
 */
public class Publicacion {

    private String id;
    private String titulo;
    private String cantidad;
    private String descripcion;
    private String usuario_id;
    private String nombreUsuario;
    private String telefono;

    public Publicacion() {
    }

    public Publicacion(String id, String imagen, String disponibilidad, String descripcion, String usuario_id, String nombreUsuario, String telefono) {
        this.id = id;
        this.titulo = imagen;
        this.cantidad = disponibilidad;
        this.descripcion = descripcion;
        this.usuario_id = usuario_id;
        this.nombreUsuario = nombreUsuario;
        this.telefono = telefono;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(String usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
