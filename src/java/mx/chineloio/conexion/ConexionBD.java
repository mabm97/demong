/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.chineloio.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author k-m-f
 */
public class ConexionBD {

    Connection conectar = null;

    /**
     * Método publico static de tipo Connection encargado de realizar la
     * conexión a la base de datos.
     *
     * @return Conexión a la base de datos de Mysql.
     * @throws SQLException Error en la conexión a la base de datos.
     */
    public Connection getConexion() throws SQLException {
        try {
            Class.forName("org.gjt.mm.mysql.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
        //return DriverManager.getConnection("jdbc:mysql://sql173.main-hosting.eu/u562514642_sinve", "u562514642_felix", "marco12345");
        return DriverManager.getConnection("jdbc:mysql://localhost/ongs?useSSL=false", "root", "root");
    }

    public static void main(String[] args) throws SQLException {
        ConexionBD c= new ConexionBD();
        if (c.getConexion() != null) {
            System.out.println("Exito");
        } else {
            System.out.println("Error");
        }
    }

}
