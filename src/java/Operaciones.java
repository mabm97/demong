/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mx.chineloio.dao.PublicacionDao;
import mx.chineloio.dao.UsuarioDao;
import mx.chineloio.modelo.Publicacion;
import mx.chineloio.modelo.Usuario;

/**
 *
 * @author k-m-f
 */
@WebServlet(urlPatterns = {"/Operaciones"})
public class Operaciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Gson g = new Gson();

        HttpSession httpSession = request.getSession();

        String opt = (String) request.getParameter("opt");
        if (opt.equals("1")) {// Login
            UsuarioDao ud = new UsuarioDao();
            String email = request.getParameter("email");
            String contrasena = request.getParameter("contrasena");
            Usuario us = null;
            try {
                us = ud.iniciarSesion(email, contrasena);
                if (us != null) {
                    httpSession.setAttribute("idUser", us.getId());
                }
                out.write(g.toJson(us));
            } catch (Exception e) {
                out.write(g.toJson(e.getMessage()));
            }
        } else if (opt.equals("2")) {//registrar usuario
            UsuarioDao ud = new UsuarioDao();
            String us = request.getParameter("usuario");
            Usuario u = g.fromJson(us, Usuario.class);
            out.write(g.toJson(ud.saveUsuario(u)));
        } else if (opt.equalsIgnoreCase("3")) {
            String publicacionJson = (String) request.getParameter("publicacion");
            Publicacion obj = g.fromJson(publicacionJson, Publicacion.class);
            PublicacionDao dao = new PublicacionDao();
            obj.setUsuario_id((String) httpSession.getAttribute("idUser"));
            out.write(g.toJson(dao.crearPublicacion(obj)));
        } else if (opt.equalsIgnoreCase("4")) {
            PublicacionDao dao = new PublicacionDao();
            String idUsuario = (String) httpSession.getAttribute("idUser");
            ArrayList<Publicacion> res = dao.obtenerPublicaciones(idUsuario);
            out.write(g.toJson(res));
        } else if (opt.equalsIgnoreCase("5")) {
            PublicacionDao dao = new PublicacionDao();
            String id = (String) request.getParameter("idAds");
            Boolean res = dao.quitarPublicacion(id);
            out.write(g.toJson(res));
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Operaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
