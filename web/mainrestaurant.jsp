<!--
=========================================================
 Paper Dashboard 2 - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard-2
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->



<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="../assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            WASTEHEROES
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="./assetsrestaurant/css/bootstrap.min.css" rel="stylesheet" />
        <link href="./assetsrestaurant/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="./assetsrestaurant/demo/demo.css" rel="stylesheet" />
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="sidebar" data-color="white" data-active-color="danger">
                <!--
                  Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
                -->
                <div class="logo">
                    <a href="./mainrestaurants.jsp" class="simple-text logo-mini">
                    </a>
                    <a href="./mainrestaurants.jsp" class="simple-text logo-normal">
                        <div class="logo-image-big">
                            <img style ="width: 120px; height: 120px; " src="./assets/img/HasteHeroes.png">
                            <!--WASTEHEROES-->
                        </div>
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li  >
                            <a data-toggle="modal" data-target="#exampleModal">
                                <i class="nc-icon nc-pin-3"></i>
                                <p>Create Ad</p>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="modal">
                                <i class="nc-icon nc-pin-3"></i>
                                <p>Active Ads</p>
                            </a>
                        </li>
                        <div id="divAds">
                            
                        </div>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="./mainrestaurant.jsp">WASTEHEROES</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link btn-rotate" href="./login.jsp">
                                        <i class="nc-icon nc-settings-gear-65"></i>
                                        <p>
                                            <span class="d-lg-none d-md-block">Account</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <!-- <div class="panel-header panel-header-lg">
          
            <canvas id="bigDashboardChart"></canvas>
          
          
          </div> -->
                <div class="content">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5 col-md-4">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="nc-icon nc-money-coins text-success"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 col-md-8">
                                            <div class="numbers">
                                                <p class="card-category">Costs Recovered</p>
                                                <p class="card-title">$ 16,500.00
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5 col-md-4">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="nc-icon nc-delivery-fast text-danger"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 col-md-8">
                                            <div class="numbers">
                                                <p class="card-category">Donated Food</p>
                                                <p class="card-title">550 Kg
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5 col-md-4">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="nc-icon nc-favourite-28 text-primary"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 col-md-8">
                                            <div class="numbers">
                                                <p class="card-category">People Helped</p>
                                                <p class="card-title">220
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5 col-md-4">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="nc-icon nc-refresh-69 text-success"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 col-md-8">
                                            <div class="numbers">
                                                <p class="card-category">Food Waste Decreased</p>
                                                <p class="card-title">-4%
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-4">
                            <div class="card card-stats">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-5 col-md-4">
                                            <div class="icon-big text-center icon-warning">
                                                <i class="nc-icon nc-chart-bar-32 text-success"></i>
                                            </div>
                                        </div>
                                        <div class="col-7 col-md-8">
                                            <div class="numbers">
                                                <p class="card-category">Ranking in Your City</p>
                                                <p class="card-title">9th
                                                <p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    <div class="row">
                        <div class="col-md-12">
                            <div class="card ">
                                <div class="card-header ">
                                    <h5 class="card-title">Users Behavior</h5>
                                    <p class="card-category">Last Years</p>
                                </div>
                                <div class="card-body ">
                                    <canvas id=chartHours width="400" height="100"></canvas>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        <i class="fa fa-circle text-warning"></i> Feb
                                        <i class="fa fa-circle text-danger"></i> Mar
                                        <i class="fa fa-circle text-success"></i> Apr
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card ">
                                <div class="card-header ">
                                    <h5 class="card-title">Wasted Food By Type</h5>
                                    <p class="card-category">Last Month</p>
                                </div>
                                <div class="card-body ">
                                    <canvas id="chartEmail"></canvas>
                                </div>
                                <div class="card-footer ">
                                    <div class="legend">
                                        <i class="fa fa-circle text-primary"></i> Tortillas
                                        <i class="fa fa-circle text-warning"></i> Vegatables
                                        <i class="fa fa-circle text-danger"></i> Bread
                                        <i class="fa fa-circle text-success"></i> Meat
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="card card-chart">
                                <div class="card-header">
<!--                                    <h5 class="card-title">NGOs</h5>
                                    <p class="card-category">Contacting you</p>-->
                                </div>
                                <div class="card-body">
                                    <canvas id="speedChart" width="400" height="100"></canvas>
                                </div>
                                <div class="card-footer">
                                    <div class="chart-legend">
                                        <i class="fa fa-circle text-info"></i> Donated Food Last Months
                                        <i class="fa fa-circle text-warning"></i> Wasted Food Last Months
                                    </div>
                                    <hr/>
<!--                                    <div class="card-stats">
                                        <i class="fa fa-check"></i> Data information certified
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-primary btn-lg btn-block">See Full Data Of Your Donations</button>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Create Food Ad</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">


                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Title Food</span>
                                    </div>
                                    <input id="inputAddTitleFood" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>
                                <span class="input-group-text" id="inputGroup-sizing-sm">Descriptions:</span>
                                <textarea class="form-control" rows="5" id="inputAddCommentFood"></textarea>
                                <br>
                                <div class="input-group col-md-10 pr-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Quantity</span>
                                    </div>
                                    <input id="inputQuantityAddFood" type="number" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">   
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    
                                <!--</div>-->
                                <!--<div class="input-group col-md-9 pr-1">-->
                                    <!--<div class="form-group">-->
                                        <label for="exampleFormControlSelect1">Unit</label>
                                        <select class="custom-select">
                                            <!--<option selected>Unit</option>-->
                                            <option value="1">Kg.</option>
                                            <option value="2">Ltr</option>
                                            <option value="3">Pieces</option>
                                        </select>
                                    <!--</div>-->
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Cost per Unit</span>
                                    </div>
                                    <input id="inputAddTitleFood" type="number" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
                                </div>


                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-secondary"data-dismiss="modal" style="color: white" >Close</a>
                                <button onclick="saveAds()" class="btn btn-primary" >Save</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--   Core JS Files   -->
        <script src="./assetsrestaurant/js/core/jquery.min.js"></script>
        <script src="./assetsrestaurant/js/core/popper.min.js"></script>
        <script src="./assetsrestaurant/js/core/bootstrap.min.js"></script>
        <script src="./assetsrestaurant/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <!-- <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
        <!-- Chart JS -->
        <script src="./assetsrestaurant/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="./assetsrestaurant/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="./assetsrestaurant/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
        <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="./assetsrestaurant/demo/demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <!--<script src="./assets/js/plugins/sweetalert2.js"></script>-->

        <script>
        $(document).ready(function () {
            demo.initChartsPages();
            getAds();
                 $('#btnQuitarAds').on("click", function() {
                    var id = $(this).attr('id');
                    console.log(id);
                });
        });
        </script>

        <script>
            function saveAds() {
//               


            Swal.fire({
              title: 'Are you sure?',
//              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, save it!'
            }).then((result) => {
              if (result.value) {
//          


                var titulo = $("#inputAddTitleFood").val();
                var descripcion = $("#inputAddCommentFood").val();
                var quantity = $("#inputQuantityAddFood").val();
                var publicacion = new Object();
                publicacion.titulo = titulo;
                publicacion.descripcion = descripcion;
                publicacion.cantidad = quantity;
                
                 $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "./Operaciones",
                    data: "publicacion=" +JSON.stringify(publicacion) + "&opt=" + 3
                }).done(function (data) {
                    console.log(data);
                    if (data) {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: 'Your ad has been published',
                            showConfirmButton: false,
                            timer: 1500
                       });
                       getAds();
                    } else {
                        Swal.fire({
                           title: 'Deleted!',
                            text: 'System Error.',
                            type: 'warning',
                            showConfirmButton: false,
                            timer: 1500,
                        });
                    }
                }).fail(function () {
                    alert("Algo sali� mal");
                });
                    /*.always(function () {
                        alert("Siempre se ejecuta")
                    })*/
                }
            });

            }
        </script>
         <script>
           function removeAds(id){
               
                Swal.fire({
              title: 'Are you sure Delete it?',
//              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
              if (result.value) {
                    $.ajax({
                           method: "POST",
                           dataType: "json",
                           url: "./Operaciones",
                           data: "idAds="+id+"&opt=" + 5
                       }).done(function (data) {
                           if (data) {
                                Swal.fire({
                                 position: 'center',
                                 type: 'success',
                                 title: 'Your ads has been deleted',
                                 showConfirmButton: false,
                                 timer: 1500
                                });    
                                getAds();
                            } else {
                               Swal.fire({
                                  title: 'Deleted!',
                                   text: 'System Error.',
                                   type: 'warning',
                                   showConfirmButton: false,
                                   timer: 1500,
                               });
                           }
                       }).fail(function () {
                           alert("Algo sali� mal");
                       });
           
            }
            });
                        }

        </script>
        
        <script>
            
            function getAds(){
                $.ajax({
                           method: "POST",
                           dataType: "json",
                           url: "./Operaciones",
                           data: "opt=" + 4
                       }).done(function (data) {
                           console.log(data);
                           if (data!= null) {
                               var text ='';
                               for(var i = 0 ; i < data.length ; i++){
                                   console.log(i);
                                   text += '<li style="border-bottom: 1px solid #ccc; "><a><i class="nc-icon nc-delivery-fast"></i><label>'+data[i].titulo+'</label><br><p><label>'+data[i].descripcion+' <br> Quantity: '+data[i].cantidad+' Kg.       <a id="btnQuitarAds" onclick="removeAds('+data[i].id+')"><i class="nc-icon nc-simple-remove"></i></a>       </label></p></a></li>';
                               }
                               $("#divAds").html(text);
                               $("#inputAddTitleFood").val('');
                               $("#inputAddCommentFood").val('');
                               $("#inputQuantityAddFood").val('');
                               
                            } else {
                               Swal.fire({
                                  title: 'Deleted!',
                                   text: 'System Error.',
                                   type: 'warning',
                                   showConfirmButton: false,
                                   timer: 1500,
                               });
                           }
                       }).fail(function () {
                           alert("Algo sali� mal");
                       });
                
            }
        
        </script>
        
       
        
        
    </body>

</html>
