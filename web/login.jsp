<%-- 
    Document   : login
    Created on : 18/10/2019, 09:48:20 PM
    Author     : santi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="./assets/js/plugins/sweetalert2.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    <style type="text/css">
        .login-form {
            width: 340px;
            margin: 50px auto;
        }

        .login-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }

        .login-form h2 {
            margin: 0 0 15px;
        }

        .form-control,
        .btn {
            min-height: 38px;
            border-radius: 2px;
        }

        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#alertaId").hide();
            $("#btnLogin").click(function () {
                var email = $("#usuarios").val();
                var contrasena = $("#pass").val();
                $.ajax({
                    method: "POST",
                    dataType: "json",
                    url: "./Operaciones",
                    data: "email=" + email + "&contrasena=" + contrasena + "&opt=" + 1
                }).done(function (data) {
                    console.log(data);
                    if (data.id != null) {
                        localStorage.setItem('idUsuario', data.id);
                        if (data.idRole == 1) { //ONG
                            window.location = "./maps.jsp";
                        }else{//Restaurante
                            window.location = "./mainrestaurant.jsp";
                        }
                    } else {
                        $("#alertaId").show();
                        setTimeout(function () {
                            $("#alertaId").fadeOut(1500);
                        }, 3000);
                    }
                }).fail(function () {
                    alert("Algo salió mal");
                })
                    /*.always(function () {
                        alert("Siempre se ejecuta")
                    })*/
                    ;


            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnGuardarUsuario").click(function () {
                
//                $("#inputRolFactory").click(function(){
                    var isFactory = $("#inputRolFactory").prop("checked");
//                });
//                $("#inputRolOng").click(function(){
                    var isOng = $("#inputRolOng").prop("checked");
//                });
                console.log('factory ' +isFactory);
                console.log('ONG  '  +isOng);
                
                var arr = $('#' + 'formAgregarUsuario').serializeArray();
                console.log(arr);

                var obj = new Object();
                obj.nombre = arr[0].value;
                obj.email = arr[1].value;
                obj.contrasena = arr[2].value;
                obj.telefono = arr[3].value;
                obj.direccion = arr[4].value;
                if (isFactory) {
                    obj.idRole = 2;
                }else if(isOng){
                    obj.idRole = 1;
                }
//                obj.idRole = arr[4].value;

                $.ajax({
                        method: "POST",
                        dataType: "json",
                        url: "./Operaciones",
                        data: "usuario=" + JSON.stringify(obj) + "&opt=" + 2
                    }).done(function (data) {

                        console.log(data);
                        if (data) {
                            Swal.fire({
                                type: 'success',
                                title: 'Registro Correcto',
                                text: '',
                                showConfirmButton: false,
                                timer: 2000
                            });
                            $('#exampleModalCenter').modal('hide')
                        } else {
                            Swal.fire({
                                type: 'error',
                                title: 'Ocurrió un Error en el Registro',
                                text: '',
                                showConfirmButton: false,
                                timer: 2000
                            });
                        }
                    }).fail(function () {
                        alert("Algo salió mal");
                    })
                });
        });
    </script>

</head>

<body>
    <div class="login-form">
        <form>
            <h2 class="text-center">WasteHeroes</h2>
            <div class="form-group">
                <input id="usuarios" type="text" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
                <input id="pass" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <a id="btnLogin" class="btn btn-primary btn-block">Log in</a>
            </div>
            <div id="alertaId" class="alert alert-danger" role="alert">
                Email/Password Incorrect
            </div>
        </form>
        <p class="text-center"><a data-toggle="modal" data-target="#exampleModalCenter">Create New Account</a></p>
    </div>






    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Create Account</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form id="formAgregarUsuario">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Name</label>
                            <input type="text" class="form-control" id="inputNombre" name="inputNombre"
                                placeholder="Granjas el Pollon/ONG">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Email</label>
                            <input type="email" class="form-control" id="inputEmail" name="inputEmail"
                                placeholder="ejemplo@email.com">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Password</label>
                            <input type="password" class="form-control" id="inputContrasena" name="inputContrasena">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Telephone</label>
                            <input type="numeric" class="form-control" id="inputTelefono" name="inputTelefono">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Address</label>
                            <input type="text" class="form-control" id="inputAddress" name="inputAddress">
                        </div>
                        <label>I am:</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="inputRolOng"
                                name="inputRol" value="1" checked>
                            <label class="form-check-label" for="exampleRadios1">
                                ONG
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="exampleRadios" id="inputRolFactory" value="2">
                            <label class="form-check-label" for="exampleRadios2">
                                Factory
                            </label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button id="btnGuardarUsuario" type="button" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>









</body>

</html>