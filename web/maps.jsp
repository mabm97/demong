<%-- 
    Document   : maps
    Created on : 18/10/2019, 09:52:30 PM
    Author     : santi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    WasteHeroes
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
    name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css"
    href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="./assets/css/material-dashboard.css?v=2.1.1" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="./assets/demo/demo.css" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="./assets/img/sidebar-1.jpg">
      <div class="logo">
        <a href="./maps.jsp" class="simple-text logo-normal">
          WasteHeroes
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item  ">
            <a class="nav-link" href="./main.jsp">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item active ">
            <a class="nav-link" href="./map.html">
              <i class="material-icons">location_ons</i>
              <p>Look for Food</p>
            </a>
          </li>
<!--          <li class="nav-item ">
            <a class="nav-link">
              <i class="material-icons">person</i>
              <p>Profile</p>
            </a>
          </li>-->
          <li class="nav-item ">
            <a class="nav-link">
            <h3>What are you looking for?</h3>
            
            <div class="form-group">
                <!--<label for="exampleFormControlSelect1"></label>-->
                <select class="custom-select">
                    <!--<option selected>Open this select menu</option>-->
                    <option value="1">Prepared Food</option>
                    <option value="2">Tortillas</option>
                    <option value="3">Vegetables</option>
                    <option value="4">Meat</option>
                    <option value="5">Bread</option>
                    <option value="6">Seeds</option>
                </select>
            </div>

<!--            <p>Tortillas</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 35%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Tortillas</div>
            </div>
            <p>Vegetables</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 35%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">Vegetables</div>
            </div>
            <p>Meat</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">Meat</div>
            </div>
            <p>Bread</p>
            <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">Bread</div>
            </div>-->
            
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="./maps.js">Look for Food</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item dropdown">
                <a class="nav-link" href="./main.jsp" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item">Profile</a>
                  <a class="dropdown-item">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="./login.jsp">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div id="map"></div>




      <!-- End Navbar -->



    </div>
  </div>

  <!--   Core JS iles   -->
  <script src="./assets/js/core/jquery.min.js"></script>
  <script src="./assets/js/core/popper.min.js"></script>
  <script src="./assets/js/core/bootstrap-material-design.min.js"></script>
  <script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="./assets/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="./assets/js/plugins/sweetalert2.js"></script>
  <!-- Forms Valdations Plugin -->
  <script src="./assets/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin fr the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="./assets/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin or Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="./assets/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="./assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="./assets/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="./assets/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="./assets/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="./assets/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="./assets/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="./assets/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDMX-H_JeH7WDpVT-vlvsrUlx_HIZf0Uk"></script>
  <!-- Chartist JS -->
  <script src="./assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="./assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/material-dashboard.js?v=2.1.1" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="./assets/demo/demo.js"></script>
  <script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

  <!-- MAPA !!!!!!!!!!!!!--->



  <script>
    $(document).ready(function () {


      $().ready(function () {
        $sidebar = $('.sidebar');
        $sidebar_img_container = $sidebar.find('.sidebar-background');
        $full_page = $('.full-page');
        $sidebar_responsive = $('body > .navbar-collapse');
        window_width = $(window).width();
        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();
        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }
        }
        $('.fixed-plugin a').click(function (event) {
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });
        $('.fixed-plugin .active-color span').click(function () {
          $full_page_background = $('.full-page-background');
          $(this).siblings().removeClass('active');
          $(this).addClass('active');
          var new_color = $(this).data('color');
          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }
          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }
          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });
        $('.fixed-plugin .background-color .badge').click(function () {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');
          var new_color = $(this).data('background-color');
          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });
        $('.fixed-plugin .img-holder').click(function () {
          $full_page_background = $('.full-page-background');
          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');
          var new_image = $(this).find("img").attr('src');
          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function () {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }
          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
            $full_page_background.fadeOut('fast', function () {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }
          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');
            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }
          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });
        $('.switch-sidebar-image input').change(function () {
          $full_page_background = $('.full-page-background');
          $input = $(this);
          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function () {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function () {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function () {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function () {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>
  <script>
    $(document).ready(function () {

      var demo2 = {

        initGoogleMaps: function () {
        /*  
        var myLatlng = new google.maps.LatLng(19.4326009, -99.1333416);
          var mapOptions = {
            zoom: 13,
            center: myLatlng,
            scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
            styles: [{
              "featureType": "water",
              "stylers": [{
                "saturation": 43
              }, {
                "lightness": -11
              }, {
                "hue": "#0088ff"
              }]
            }, {
              "featureType": "road",
              "elementType": "geometry.fill",
              "stylers": [{
                "hue": "#ff0000"
              }, {
                "saturation": -100
              }, {
                "lightness": 99
              }]
            }, {
              "featureType": "road",
              "elementType": "geometry.stroke",
              "stylers": [{
                "color": "#808080"
              }, {
                "lightness": 54
              }]
            }, {
              "featureType": "landscape.man_made",
              "elementType": "geometry.fill",
              "stylers": [{
                "color": "#ece2d9"
              }]
            }, {
              "featureType": "poi.park",
              "elementType": "geometry.fill",
              "stylers": [{
                "color": "#ccdca1"
              }]
            }, {
              "featureType": "road",
              "elementType": "labels.text.fill",
              "stylers": [{
                "color": "#767676"
              }]
            }, {
              "featureType": "road",
              "elementType": "labels.text.stroke",
              "stylers": [{
                "color": "#ffffff"
              }]
            }, {
              "featureType": "poi",
              "stylers": [{
                "visibility": "off"
              }]
            }, {
              "featureType": "landscape.natural",
              "elementType": "geometry.fill",
              "stylers": [{
                "visibility": "on"
              }, {
                "color": "#b8cb93"
              }]
            }, {
              "featureType": "poi.park",
              "stylers": [{
                "visibility": "on"
              }]
            }, {
              "featureType": "poi.sports_complex",
              "stylers": [{
                "visibility": "on"
              }]
            }, {
              "featureType": "poi.medical",
              "stylers": [{
                "visibility": "on"
              }]
            }, {
              "featureType": "poi.business",
              "stylers": [{
                "visibility": "simplified"
              }]
            }]
          }
        */
        
        

            var locations = [
                ['Quattro', 19.3673155,-99.2391494],
                ['Loretta Chic Bistrot', 19.3656888,-99.2378078],
                ['Eloise Chic Cuisine', 19.3579959,-99.2254481],
                ['El Cardenal', 19.3472371,-99.1907669],
                ['La Taberna del León', 19.3472371,-99.1907669],
                ['Los Danzantes Coyoacán', 19.3390301,-99.1937434],
                ['Café La Pagoda', 19.4346659,-99.1417353],
                ['5M', 19.4346659,-99.1417353],
                ['Restaurante Don Toribio', 19.4336508,-99.139786],
                ['Casino Español de México', 19.4336508,-99.139786],
                ['Café de Tacuba', 19.4355912,-99.1380816],
                ['El Pescadito', 19.3385845,-99.1779267],
                ['Salicornia, Placeres de Ensenada', 19.4144261,-99.1779267],
                ['Creperie de la Paix', 19.418579,-99.1779267],
                ['Asian Bay', 19.414968,-99.1779267],
                ['Ojo de Agua', 19.4265622,-99.165297],
                ['Restaurante "Toño y Julio', 19.4199617,,-99.1401161],
                ['Restaurante Bar Villahermosa', 19.4191296,-99.1401161],
                ['La Mansión', 19.3936402,-99.1619834]
//                ['Los Arcos Restaurant', 19.3385845,-99.1963114],
//                ['Los Arcos Restaurant', 19.3385845,-99.1963114],
//                ['Los Arcos Restaurant', 19.3385845,-99.1963114],
            ];
        
        
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: new google.maps.LatLng(19.4326009, -99.1333416),
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });

          var infowindow = new google.maps.InfoWindow();
          var marker, i;
          for (i = 0; i < locations.length; i++) {  
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
              }
            })(marker, i));
          }
        
        
        
        
        
        
//        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
          
          
          

          
    
          
    
    
    
    
    
    
    }
      };


      demo2.initGoogleMaps();















      ///////////////////////c
      /*
      $.ajax({
          url : 'http://10.49.177.111:8080/rest/service/licencia',
          type : 'GET',
          crossDomain: true,
          dataType : 'json',
          success : function(json) {
            var demo2 = {
            initGoogleMaps: function() {
              var myLatlng = new google.maps.LatLng(18.9299863802915, -99.25094731970849);
              var mapOptions = {
              zoom: 13,
                center: myLatlng,
                scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
                styles: [{
                  "featureType": "water",
                  "stylers": [{
                    "saturation": 43
                  }, {
                    "lightness": -11
                  }, {
                    "hue": "#0088ff"
                  }]
                }, {
                  "featureType": "road",
                  "elementType": "geometry.fill",
                  "stylers": [{
                    "hue": "#ff0000"
                  }, {
                    "saturation": -100
                  }, {
                    "lightness": 99
                  }]
                }, {
                  "featureType": "road",
                  "elementType": "geometry.stroke",
                  "stylers": [{
                    "color": "#808080"
                  }, {
                    "lightness": 54
                  }]
                }, {
                  "featureType": "landscape.man_made",
                  "elementType": "geometry.fill",
                  "stylers": [{
                    "color": "#ece2d9"
                  }]
                }, {
                  "featureType": "poi.park",
                  "elementType": "geometry.fill",
                  "stylers": [{
                    "color": "#ccdca1"
                  }]
                }, {
                  "featureType": "road",
                  "elementType": "labels.text.fill",
                  "stylers": [{
                    "color": "#767676"
                  }]
                }, {
                  "featureType": "road",
                  "elementType": "labels.text.stroke",
                  "stylers": [{
                    "color": "#ffffff"
                  }]
                }, {
                  "featureType": "poi",
                  "stylers": [{
                    "visibility": "off"
                  }]
                }, {
                  "featureType": "landscape.natural",
                  "elementType": "geometry.fill",
                  "stylers": [{
                    "visibility": "on"
                  }, {
                    "color": "#b8cb93"
                  }]
                }, {
                  "featureType": "poi.park",
                  "stylers": [{
                    "visibility": "on"
                  }]
                }, {
                  "featureType": "poi.sports_complex",
                  "stylers": [{
                    "visibility": "on"
                  }]
                }, {
                  "featureType": "poi.medical",
                  "stylers": [{
                    "visibility": "on"
                  }]
                }, {
                  "featureType": "poi.business",
                  "stylers": [{
                    "visibility": "simplified"
                  }]
                }]
              };
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            for (let i = 0; i < json.length; i++) {  
              var direccion = ''+json[i].numero+'+'+json[i].calle+',+'+json[i].colonia+'+Morelos'
              var actividad = json[i].actividadPrincipal;
              var j = i;
              $.ajax({
                  url : 'https://maps.googleapis.com/maps/api/geocode/json?new_forward_geocoder=true&address='+direccion+'&key=AIzaSyCDMX-H_JeH7WDpVT-vlvsrUlx_HIZf0Uk',
                  type : 'GET',
                  crossDomain: true,
                  dataType : 'json',
                  success : function(json2,i) {
                    marker = new google.maps.Marker({
                      position: new google.maps.LatLng(json2.results[0].geometry.location.lat,json2.results[0].geometry.location.lng),
                      map: map
                    });
                    var infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                      return function() {
                        infowindow.setContent('<div><h3>La Casa del Herrero</h3><br><img src="./imgs/lugar.PNG" alt="La Casa del Herrero" style="width:300px;height:200px;"><br><span class="heading">Calificación</span><span class="fa fa-star"></span><span class="fa fa-star checked"></span><span class="fa fa-star "></span><span class="fa fa-star"></span><span class="fa fa-star"></span> <p>Calle Cipres No.325, Col.Teopanzolco, Cuernavaca, Morelos</p> <br><a onclick="openModal()">Evaluar</a> </div>');
                        infowindow.open(map, marker);
                      }
                    })(marker, j));
                  },
                  error : function(xhr, status) {
                      alert('Disculpe, existió un problema');
                  }
              });
            }//for
            }// fin function
          }// fin demo
         demo2.initGoogleMaps();
        },
          error : function(xhr, status) {
              alert('Disculpe, existió un problema');
          }
        });
      */
    });
  </script>s

  <script>
    function openModal() {
      /*
          $.ajax({
                    url: 'http://10.49.177.111:8080/rest/service/tipoIrregularidad',
                    crossDomain: true,
                    type : 'GET',
                    dataType : 'json',
                  success : function(json) {
                      console.log(json);
                    for (let i = 0; i < json.length; i++) {  
                    $("#divDocumentos").append("<div value="+json[i].idTipoIrregularidad+" class='column'>&nbsp;&nbsp;<input type='checkbox' data-toggle='toggle' data-width='100' data-height='75'><label>"+json[i].nombre+"</label></div>");
      
                    }
      
      
      
                },
                error : function(xhr, status) {
                    alert('Disculpe, existió un problema');
                }
            });
      
      
             $("#modalMaps").modal("show");
      */
    }
  </script>
</body>

</html>